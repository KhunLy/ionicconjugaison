import { Component, OnInit } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private sqlLite: SQLite,
  ) { }

  ngOnInit(): void {
    this.sqlLite.create({name: 'louloudb.db', location: 'default'})
      .then(() => console.log("La db a été crée"))
      .catch(console.log)
  }

}
